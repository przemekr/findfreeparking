#!/usr/bin/env python
#encoding: utf-8
VERSION='0.0.1'
APPNAME='pirking_finder'

top = '.'

from waflib import Configure, Logs

def options(opt):
	opt.load('compiler_cxx')
        opt.add_option('--double_click', action='store',
              default='0',
              dest='double_click')

def configure(conf):
        conf.load('compiler_cxx')
        conf.check_cfg(
                path='sdl2-config',
                args='--cflags --libs', package = '',
                uselib_store='platform')
        conf.env.PLATFORM_SRC = ['agg-2.5/src/platform/sdl2/agg_platform_support.cpp']

def build(bld):
	bld.recurse('src')
