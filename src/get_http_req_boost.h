#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <string>

using namespace std;

inline string getHttpReq(string host, string name)
{
   char buf[1024];
   string message =
      "GET /"+name+" HTTP/1.1\n"
      "Host: przemekr.sdfeu.org\n"
      "\n";
   using boost::asio::ip::tcp;
   boost::asio::io_service io_service;
   tcp::resolver resolver(io_service);
   tcp::resolver::query query(tcp::v4(), host, "http");
   tcp::resolver::iterator end;
   auto it = resolver.resolve(query);
   std::cout << it->endpoint() << "\n";
   tcp::socket socket(io_service);
   socket.connect(*it);
   boost::system::error_code ignored_error;
   boost::asio::write(socket, boost::asio::buffer(message),
         boost::asio::transfer_all(), ignored_error);
   size_t len = socket.read_some(boost::asio::buffer(buf));
   string reply(buf, len);
   return string(reply.begin()+reply.find("\r\n\r\n")+4, reply.end());

}
