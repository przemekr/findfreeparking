#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>

using namespace std;

inline string getHttpReq(string host, string name)
{
   char buffer[1024];
   int len, mysocket;
   struct sockaddr_in dest; 

   mysocket = socket(AF_INET, SOCK_STREAM, 0);
   memset(&dest, 0, sizeof(dest));                /* zero the struct */
   dest.sin_family = AF_INET;
   dest.sin_addr.s_addr = htonl(0xb23f23c2);
   dest.sin_port = htons(80);                /* set destination port number */
   connect(mysocket, (struct sockaddr *)&dest, sizeof(struct sockaddr));
 
   string message =
      "GET /"+name+" HTTP/1.1\n"
      "Host: przemekr.sdfeu.org\n"
      "\n";

   send(mysocket, message.c_str(), message.length(), 0);  
   len = recv(mysocket, buffer, sizeof(buffer), 0);
   buffer[len] = '\0';
   string reply(buffer, len);
   return string(reply.begin()+reply.find("\r\n\r\n")+4, reply.end());

}
