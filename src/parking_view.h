#include "get_http_req.h"
#include <sstream>
#include <vector>
#include <iostream>
#include <algorithm>

#define MENUBAR_HIGHT 45
#define CAR_HIGHT 60.0
#define CAR_WIDTH 100.0

class ParkingView : public AppView
{
public:
   ParkingView(App& app): AppView(app),
   menu(300,  10, 380, 30,   "MENU", !flip_y),
   refresh(400,  10, 480, 30,   "UPDATE", !flip_y),
   was_multi(false), was_move(false), downX(0), downY(0)
   {
      menu.background_color(red);
      refresh.background_color(red);
      add_ctrl(menu);
      add_ctrl(refresh);
   }

   void on_mouse_button_up(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_button_up(x, y))
      {
         app.on_ctrl_change();
         app.force_redraw();
      }
      was_multi = false;
      was_move = false;
   }

   void on_mouse_button_down(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_button_down(x, y))
      {
         app.on_ctrl_change();
         app.force_redraw();
         return;
      }
      downX = x;
      downY = y;
   }

   void on_mouse_move(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_move(x, y, (flags & agg::mouse_left) != 0))
      {
         app.on_ctrl_change();
         app.force_redraw();
         return;
      }
      if (!flags &agg::mouse_left or was_multi)
         return;
      if ((downX-x)*(downX-x)+(downY-y)*(downY-y) < 5)
         return;

      was_move = true;
      zoom_mtx *= agg::trans_affine_translation(x-downX, y-downY);
      downX=x;
      downY=y;
      app.force_redraw();
   }

   void on_ctrl_change()
   {
      if (menu.status())
      {
         menu.status(false);
         app.changeView("menu");
      }
   }

   void on_key(int x, int y, unsigned key, unsigned flags)
   {
      printf("on key %d\n", key);
      if (key == 27 || key == 0x4000010e /* Android BACK key*/)
      {
      }
      if (key == 'z')
      {
         zoom_mtx *= agg::trans_affine_translation(-x, -y);
         zoom_mtx *= agg::trans_affine_scaling(1.1);
         zoom_mtx *= agg::trans_affine_translation(x, y);
         app.force_redraw();
      }
      if (key == 'a')
      {
         zoom_mtx *= agg::trans_affine_translation(-x, -y);
         zoom_mtx *= agg::trans_affine_scaling(0.9);
         zoom_mtx *= agg::trans_affine_translation(x, y);
         app.force_redraw();
      }
      if (key == 's')
      {
         zoonToW();
      }
      if (key == 'x')
      {
         zoonToH();
      }
   }

   void on_multi_gesture(float x, float y,
         float dTheta, float dDist, int numFingers)
   {
      was_multi = true;
      if (dDist >= 0)
      {
         x *= app.rbuf_window().width();
         y *= app.rbuf_window().height();
         y = app.rbuf_window().height()-y;
         zoom_mtx *= agg::trans_affine_translation(-x, -y);
         zoom_mtx *= agg::trans_affine_scaling((1+4*dDist));
         zoom_mtx *= agg::trans_affine_translation(x, y);
         app.force_redraw();
      }
      if (dDist < -0.01)
      {
         zoom_mtx.reset();
         app.force_redraw();
      }
   }

   class Space
   {
   public:
      Space(agg::rasterizer_scanline_aa<>& inras,
            agg::scanline_u8& insl,
            renderer_base_type& inrbase,
            agg::trans_affine& inmtx):
        ras(inras), sl(insl), rbase(inrbase), mtx(inmtx)
      {
      }

      void draw(
            int x, int y,
            int xwinoff = 0,
            int ywinoff = 0,
            int xwinend = 500,
            int ywinend = 500,
            double inxsize = CAR_WIDTH, double inysize = CAR_HIGHT) 
      {
         xsize = inxsize;
         ysize = inysize;
         xoff = x*xsize;
         yoff = y*ysize;
         mtx.transform(&xoff, &yoff);
         xoff += xwinoff;
         yoff += ywinoff;
         xsize *= mtx.scale();
         ysize *= mtx.scale();
         xend = xoff+xsize;
         yend = yoff+ysize;

         if (xoff > xwinend || yoff > ywinend || xend < xwinoff || yend < ywinoff)
         {
            return;
         }

         drawSpace();
      };
   protected:
      double xoff, yoff;
      double xend, yend;
      double xsize, ysize;
      agg::rasterizer_scanline_aa<>& ras;
      agg::scanline_u8& sl;
      renderer_base_type& rbase;
      agg::trans_affine& mtx;

      virtual void drawSpace() = 0;
   };

   class Car: public Space
   {
   public:
      Car(agg::rasterizer_scanline_aa<>& inras,
            agg::scanline_u8& insl,
            renderer_base_type& inrbase,
            agg::trans_affine& inmtx,
            agg::rendering_buffer& inimg):
         Space(inras, insl, inrbase, inmtx),
         img(inimg)
      {
      }
   private:
      void drawSpace()
      {
         double ximgscale = xsize/img.width();
         double yimgscale = ysize/img.height();
         agg::trans_affine img_mtx; img_mtx.reset();
         img_mtx *= agg::trans_affine_scaling(ximgscale, yimgscale);
         img_mtx *= agg::trans_affine_translation(xoff, yoff);
         img_mtx.invert();

         agg::span_allocator<agg::rgba8> sa;
         typedef agg::span_interpolator_linear<> interpolator_type;
         typedef agg::image_accessor_clip<pixfmt_type> img_source_type;
         typedef agg::span_image_filter_rgba_2x2<img_source_type,
                 interpolator_type> span_gen_type;
         interpolator_type interpolator(img_mtx);

         pixfmt_type img_pixf(img);
         img_source_type img_src(img_pixf, agg::rgba_pre(0, 0.4, 0, 0.5));
         agg::image_filter<agg::image_filter_kaiser> filter;
         span_gen_type sg(img_src, interpolator, filter);

         ras.move_to_d(xoff,yoff);
         ras.line_to_d(xend,yoff);
         ras.line_to_d(xend,yend);
         ras.line_to_d(xoff,yend);
         agg::render_scanlines_aa(ras, sl, rbase, sa, sg);
      }
   private:
      agg::rendering_buffer& img;
   };

   class Emapty: public Space
   {
      using Space::Space;
      void drawSpace() {};
   };

   class RoadV: public Space
   {
      using Space::Space;
      void drawSpace()
      {
         renderer_scanline_type ren_sl(rbase);
         ren_sl.color(agg::rgba(0, 0, 0, 0.8));
         agg::rounded_rect r0(xoff, yoff, xend, yend, 0);
         ras.add_path(r0);
         agg::render_scanlines(ras, sl, ren_sl);
         ras.reset();

         ren_sl.color(agg::rgba(1, 1, 1, 0.8));
         agg::rounded_rect r1(xoff+0.1*xsize, yoff+0.46*ysize, xoff+0.4*xsize, yoff+0.54*ysize, 2);
         agg::rounded_rect r2(xoff+0.6*xsize, yoff+0.46*ysize, xoff+0.9*xsize, yoff+0.54*ysize, 2);
         ras.add_path(r1);
         ras.add_path(r2);
         agg::render_scanlines(ras, sl, ren_sl);
      }
   };

   class RoadH: public Space
   {
      using Space::Space;
      void drawSpace()
      {
         renderer_scanline_type ren_sl(rbase);
         ren_sl.color(agg::rgba(0, 0, 0, 0.8));
         agg::rounded_rect r0(xoff, yoff, xend, yend, 0);
         ras.add_path(r0);
         agg::render_scanlines(ras, sl, ren_sl);
         ras.reset();

         ren_sl.color(agg::rgba(1, 1, 1, 0.8));
         agg::rounded_rect r(xoff+0.48*xsize, yoff+0.1*ysize, xoff+0.52*xsize, yoff+0.9*ysize, 2);
         ras.add_path(r);
         agg::render_scanlines(ras, sl, ren_sl);
      }
   };

   void on_draw()
   {
      pixfmt_type pf(app.rbuf_window());;
      agg::renderer_base<pixfmt_type> rbase(pf);
      rbase.clear(lgray);
      agg::rasterizer_scanline_aa<> ras;
      ras.reset();
      agg::scanline_u8 sl;

      int w = app.width();
      int h = app.height();

      int xwinoff = 0;
      int ywinoff = MENUBAR_HIGHT;
      int xwinend = w;
      int ywinend = h;

      Car car(ras, sl, rbase, zoom_mtx, app.rbuf_img(1));
      Emapty empty(ras, sl, rbase, zoom_mtx);
      RoadV roadV(ras, sl, rbase, zoom_mtx);
      RoadH roadH(ras, sl, rbase, zoom_mtx);

      for (int x = 0; x < noCols; x++ )
         for (int y = 0; y < noRows; y++ )
         {
            if (rows[noRows-y-1][x] == 'X')
            {
               car.draw(x, y, xwinoff, ywinoff, xwinend, ywinend);
            }
            if (rows[noRows-y-1][x] == ' ')
            {
               if (
                     (x >=1        && rows[noRows-y-1][x-1] == ' ') or 
                     (x < noCols-1 && rows[noRows-y-1][x+1] == ' ') 
                  )
               {
                  roadV.draw(x, y, xwinoff, ywinoff, xwinend, ywinend);
               } else if (
                     (y < noRows-1 && rows[noRows-y-2][x] == ' ') or 
                     (y >= 1       && rows[noRows-y  ][x] == ' ') 
                     )
               {
                  roadH.draw(x, y, xwinoff, ywinoff, xwinend, ywinend);
               }
            }
            empty.draw(x, y, xwinoff, ywinoff, xwinend, ywinend);
         }
      rbase.blend_bar(0,0,w,MENUBAR_HIGHT, dgray, agg::cover_full);
      app.draw_text(10, 10, 40, red, 1.0, "%d", getNoLeft());
      app.draw_text(100, 10, 20, red, 1.0, " left");
      agg::render_ctrl(ras, sl, rbase, menu);
      agg::render_ctrl(ras, sl, rbase, refresh);
   }

   int getNoLeft()
   {
      int no = 0;
      for (vector<string>::iterator i = rows.begin(); i != rows.end(); i++)
      {
         no += count(i->begin(), i->end(), 'E');
      }
      return no;
   }

   void enter()
   {
      wait_mode(true);
      resized = true;
      string line;
      string output = 
"XE XE XE XE EX XX XE\n"
"XE XE EE XE XX XX XX\n"
"EE EE EE EE XX XX XX\n"
"EE EE EE EE XX XE XX\n"
"EE XE EE XE XX XX XX\n"
"EE XE EE XE XX XX XX\n"
"EE XX XX XX XX XX XX\n"
"XE XX XX XX XX XX XX\n"
"XE XX XX XX XX XX XX\n"
"                    \n"
"                    \n"
"XE XE XX XX XX XX XX\n"
"XE XE XX XX XX XX XX\n"
"XE EE XX XX XX XX EX\n"
"XE XE EE XE XX XX XX\n"
"XX XX EX XX XX XX XX\n"
"XX XE XX XE XX XX EX\n"
"XX XX EX XX XX XX XX\n"
"XE XE XE XE XX XE XX\n"
"XE XE EE XE XX XX XX\n";
      stringstream ss(output);
      rows.clear();
      while (getline(ss, line))
      {
         cout << line << "\n";
         rows.push_back(line);
      }

      noRows = rows.size();
      noCols = rows[0].length();
      frameNo = 0;
      zoonToH();
   }

   void on_resize(int, int)
   {
      resized = true;
      zoonToH();
   }
   int max_fps() { return 1; }

private:
   void zoonToH()
   {
      zoom_mtx.reset();
      double winw = app.rbuf_window().width();
      double winh = app.rbuf_window().height()-MENUBAR_HIGHT;
      double scale = winh/(noRows*CAR_HIGHT);
      zoom_mtx *= agg::trans_affine_scaling(scale);

      double parkin = scale*(noCols*CAR_WIDTH);
      double left = std::max(winw - parkin, 0.0);
      zoom_mtx *= agg::trans_affine_translation(left/2, 0);
      app.force_redraw();
   }
   void zoonToW()
   {
      zoom_mtx.reset();
      double winw = app.rbuf_window().width();
      double winh = app.rbuf_window().height()-MENUBAR_HIGHT;
      double scale = winw/(noCols*CAR_WIDTH);
      zoom_mtx *= agg::trans_affine_scaling(scale);

      double parkin = scale*(noRows*CAR_HIGHT);
      double left = std::max(winh - parkin, 0.0);
      zoom_mtx *= agg::trans_affine_translation(0, left/2);

      app.force_redraw();
   }

   void update(long elapsed_time)
   {
      frameNo++;
   }
   agg::trans_affine zoom_mtx;
   agg::button_ctrl<agg::rgba8> menu;
   agg::button_ctrl<agg::rgba8> refresh;
   int downX, downY;
   bool was_multi;
   bool was_move;
   bool resized;
   int noRows;
   int noCols;
   long frameNo;
   vector<string> rows;
};

