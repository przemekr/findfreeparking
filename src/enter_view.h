#include <sstream>
#include <vector>
#include <iostream>

class SplashView : public AppView
{
public:
   SplashView(App& app): AppView(app) {}

   void on_mouse_button_up(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_button_up(x, y))
      {
         app.on_ctrl_change();
         app.force_redraw();
      }
   }

   void on_mouse_button_down(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_button_down(x, y))
      {
         app.on_ctrl_change();
         app.force_redraw();
      }
   }

   void on_mouse_move(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_move(x, y, (flags & agg::mouse_left) != 0))
      {
         app.on_ctrl_change();
         app.force_redraw();
         return;
      }
   }

   void on_ctrl_change()
   {
   }

   void on_multi_gesture(float x, float y,
         float dTheta, float dDist, int numFingers)
   {
   }

   void on_draw()
   {
      int w = app.width();
      int h = app.height();
      app.draw_text(200, 200, 30, lblue, 1.0, "Loading...");
   }

   void enter()
   {
      wait_mode(false);
   }

   void on_resize(int, int)
   {
      resized = true;
   }
   int max_fps() { return 20; }

private:
   void update(long elapsed_time)
   {
      app.changeView("parking");
   }
   bool resized;
   int noRows;
   int noCols;
   long frameNo;
   vector<string> rows;
};

