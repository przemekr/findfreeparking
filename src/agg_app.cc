/*
 * Parking App, a simple parking application.
 * Copyright 2014 Przemyslaw Rzepecki
 * Contact: przemekr@sdfeu.org
 * 
 * This file is part of Parking App
 * 
 * AGG-parking is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 * 
 * AGG-parking is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * AGG-parking.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <algorithm> 
#include <stack>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <time.h>

#include "app_support.h"
#include "parking_view.h"
#include "enter_view.h"


class the_application: public App
{
public:
   the_application(agg::pix_format_e format, bool flip_y) :
      App(format, flip_y)
   {
      parking = new ParkingView(*this);
      splash = new SplashView(*this);
      changeView("splash");
   }
   virtual void changeView(const char* name) 
   {
      if (strcmp(name, "parking") == 0)
         view = parking;
      if (strcmp(name, "splash") == 0)
         view = splash;
      view->enter();
   };
private:
   ParkingView* parking;
   SplashView* splash;
};


int agg_main(int argc, char* argv[])
{
    the_application app(agg::pix_format_bgra32, flip_y);
    app.caption("Find Parking Place");

    if (false
          || !app.load_img(0,   "backgroud_parking.png")
          || !app.load_img(1,   "Ferrari_Berlinetta.png")
          || !app.load_music(2, "melody.ogg")
       )
    {
        char buf[256];
        sprintf(buf, "There must be files 1%s...9%s\n",
                     app.img_ext(), app.img_ext());
        app.message(buf);
        return 1;
    }

    if (app.init(START_W, START_H, WINDOW_FLAGS))
    {
       try {
          return app.run();
       } catch (...) {
          return 0;
       }
    }
    return 1;
}
