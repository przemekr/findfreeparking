
class Timer
{
   long time_left;
   long time_total;
   agg::ellipse back;
   agg::ellipse sec;
   agg::ellipse min;
   double s1;
   double s2;
   double bx;
   double by;
   double sx;
   double sy;
   double mx;
   double my;
   agg::rgba color;
   bool touchs;
   bool touchm;
   bool alarm_on;

public:
   Timer(App& application, double size1, double size2):
      app(application), s1(size1), s2(size2)
   {
      time_left = 0;
      time_total = 0;
      touchs = false;
      touchm = false;
      alarm_on = false;
      update(0);
   }

   void on_mouse_button_down(int x, int y)
   {
      int w = app.rbuf_window().width();
      if ((w*sx-x)*(w*sx-x) + (w*sy-y)*(w*sy-y) < w*s2*w*s2)
      {
         touchs = true;
      }
      if ((w*mx-x)*(w*mx-x) + (w*my-y)*(w*my-y) < w*s2*w*s2)
      {
         touchm = true;
      }
   }
   void on_mouse_button_up(int x, int y)
   {
         touchs = false;
         touchm = false;
   }
   void on_mouse_move(int x, int y)
   {
      if (!touchs && !touchm)
         return;

      int w = app.rbuf_window().width();
      double newang = atan2((y - by*w), (x - bx*w));
      alarm_on = true;

      double angs = agg::pi/2+(double)time_left/1000*2*agg::pi/60;
      double angm = agg::pi/2+(double)time_left/1000*2*agg::pi/60/60;

      if (touchs)
      {
         int i = round((angs - newang)/(2*agg::pi));
         newang += i*2*agg::pi;
         time_left += (newang - angs) * 60*1000/(2*agg::pi);
         time_total = time_left;
      }

      if (touchm)
      {
         int i = round((angm - newang)/(2*agg::pi));
         newang += i*2*agg::pi;
         int min = (int)((newang - angm)/(2*agg::pi/60));
         time_left += min * 60*1000;
         time_total = time_left;
      }
      if (time_left <= 0)
      {
         alarm_on = false;
      }
      update(0);
   }

   void draw(
         agg::rasterizer_scanline_aa<>& ras,
         agg::scanline_u8& sl,
         renderer_base_type& rbase
         )
   {
      int w = app.width();
      int h = app.height();

      agg::rgba lgreen(0.1, 0.6, 0.1, 0.5);
      agg::rgba dred(0.6, 0.1, 0.1, 0.8);

      renderer_scanline_type ren_sl(rbase);

      ren_sl.color(lgreen);
      ras.reset();
      back.init(w*bx, w*by, w*s1, w*s1, 60);
      ras.add_path(back);
      agg::render_scanlines(ras, sl, ren_sl);

      ras.reset();
      agg::rgba dgreen(0.0, 0.3, 0.0, 0.8);
      ren_sl.color(dgreen);
      agg::ellipse e_out(w*bx, w*by, 1.1*w*s1, 1.1*w*s1, 60);
      agg::ellipse e_in (w*bx, w*by,   1*w*s1,   1*w*s1, 60, true);
      ras.add_path(e_out);
      ras.add_path(e_in);
      agg::render_scanlines(ras, sl, ren_sl);

      if (time_left && time_total)
      {
         ras.reset();
         agg::rgba dgreen(0.0, 0.6, 0.0, 0.6);
         ren_sl.color(dgreen);
         double perc_left = (double)time_left/time_total;
         double ang1 = agg::pi/2;
         double ang2 = agg::pi/2 + agg::pi*2*(1-perc_left);

         agg::arc arc1(w*bx, w*by, w*s1, w*s1, ang1, ang1+agg::pi);
         agg::arc arc2(w*bx, w*by, w*s1*0.9, w*s1*0.9, ang2, ang2+agg::pi, false);
         ras.add_path(arc1);
         ras.add_path(arc2);
         agg::render_scanlines(ras, sl, ren_sl);
      }

      /* draw the hour marks */
      ras.reset();
      agg::rgba dblue(0.0, 0.0, 0.6, 0.6);
      ren_sl.color(dblue);
      agg::ellipse mark(w*bx, w*(by+s1),   w*s2/8,   w*s2/2, 10);
      typedef agg::path_base<
         agg::vertex_stl_storage<
         agg::pod_auto_vector<
         agg::vertex_d, 200> > > path_storage_type;
      agg::trans_affine rotate; rotate.reset();
      for (int i = 0; i < 12; i++)
      {
         rotate.reset();
         rotate *= agg::trans_affine_translation(-bx*w, -by*w);
         rotate *= agg::trans_affine_rotation(i*2*agg::pi/12);
         rotate *= agg::trans_affine_translation(bx*w, by*w);

         path_storage_type path;
         path.concat_path(mark);
         path.transform(rotate);
         ras.add_path(path);
         agg::render_scanlines(ras, sl, ren_sl);
      }


      ren_sl.color(lblue);
      ras.reset();
      sec.init(w*sx, w*sy, w*s2, w*s2, 30);
      ras.add_path(sec);
      agg::render_scanlines(ras, sl, ren_sl);

      ras.reset();
      ren_sl.color(dred);
      min.init(w*mx, w*my, w*s2, w*s2, 30);
      ras.add_path(min);
      agg::render_scanlines(ras, sl, ren_sl);

      int minutest = time_left/60/1000;
      double sec   = (double)(time_left - minutest*60*1000)/1000;
      agg::rgba blue(0.0, 0, 0.9, 0.8);
      app.draw_text(w/2-w/10, h/2-h/10, w/20, blue, 1.0,
            "%02d:%.1f", minutest, sec);
   }

   void update(long ms)
   {
      if (!touchs && !touchm && time_left)
      {
         time_left -= ms;
      }

      if (time_left <= 0)
      {
         time_left = 0;
      }
      double angs = agg::pi/2+(double)time_left/1000*2*agg::pi/60;
      double angm = agg::pi/2+(double)time_left/1000*2*agg::pi/60/60;

      bx = 0.5;
      by = 0.5*AR;

      sx = bx + 0.95*s1*cos(angs);
      sy = by + 0.95*s1*sin(angs);

      mx = bx + 0.55*s1*cos(angm);
      my = by + 0.55*s1*sin(angm);

      if (!touchs && !touchm && !time_left && alarm_on)
      {
         alarm_on = false;
         app.changeView("alarm");
      }
   }

private:
   App& app;
};

class TimerView : public AppView
{
public:
   TimerView(App& app): AppView(app),
   menu(80,  20, 180, 40,   "Menu", !flip_y),
   timer(app, 0.4, 0.05)
   {
      menu.background_color(red);
      add_ctrl(menu);
      resized = true;
   }

   void on_mouse_button_up(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_button_up(x, y))
      {
         app.on_ctrl_change();
         app.force_redraw();
      }
      timer.on_mouse_button_up(x, y);
   }

   void on_mouse_button_down(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_button_down(x, y))
      {
         app.on_ctrl_change();
         app.force_redraw();
      }
      timer.on_mouse_button_down(x, y);
   }

   void on_mouse_move(int x, int y, unsigned flags)
   {
      if (m_ctrls.on_mouse_move(x, y, (flags & agg::mouse_left) != 0))
      {
         app.on_ctrl_change();
         app.force_redraw();
         return;
      }
      timer.on_mouse_move(x, y);
   }

   void on_ctrl_change()
   {
      if (menu.status())
      {
         menu.status(false);
         app.changeView("menu");
      }
   }

   void on_multi_gesture(float x, float y,
         float dTheta, float dDist, int numFingers)
   {
   }

   void on_draw()
   {
      pixfmt_type pf(app.rbuf_window());;
      agg::renderer_base<pixfmt_type> rbase(pf);
      agg::rasterizer_scanline_aa<> ras;
      agg::scanline_u8 sl;
      ras.reset();
      rbase.clear(black);

      decorator->background(ras, sl, rbase, resized);
      resized = false;
      timer.draw(ras, sl, rbase);
      decorator->forground(ras, sl, rbase);

      agg::render_ctrl(ras, sl, rbase, menu);
   }

   void enter()
   {
      wait_mode(false);
   }

   void on_resize(int, int)
   {
      resized = true;
   }
   int max_fps() { return 20; }

private:
   void update(long elapsed_time)
   {
      timer.update(elapsed_time);
      decorator->update(elapsed_time);
   }
   agg::button_ctrl<agg::rgba8> menu;
   Timer timer;
   bool resized;
};

